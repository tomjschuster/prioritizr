# prioritizr [![pipeline status](https://gitlab.com/tomjschuster/prioritizr/badges/master/pipeline.svg)](https://gitlab.com/tomjschuster/prioritizr/commits/master)

## Try

**Deployed App**

[https://prioritizr-c3998.firebaseapp.com/](https://prioritizr-c3998.firebaseapp.com/)

**Run Locally**

1. install node.js
2. clone repo
3. `npm install`
4. `npm start`

Once the build finishes, your browser should open to http://localhost:8080

## Usage

**Log in**

To log in, you will need to create a [GitHub personal access token](https://github.com/settings/tokens) with at least access to public repos (private if you'd like to use prioritizr for private repos as well).

Once you log in, the token will be stored in `localStorage` until you log out, at which point it will erased, so be sure to save your token somewhere safe and to not leave an active session on a computer where others have access.

**Prioritize**

After logging in, you will see a list of all repos that in the scope of your login token. Select a repo to see the list of issues and prioritize the ones most important to you by moving them up in the list. This priority will be stored in `localStorage` even after closing the browser or logging out. Anytime you visit a repo, any new issues will be added to the end of the list automatically.

## Future Enhancements

1. Select issue and move to any point in the list without
2. Drag & drop issues
3. Filter issues/repos by association/assignees
4. More tests
5. Extract common UI functionality to common framework
