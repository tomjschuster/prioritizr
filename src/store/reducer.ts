import { Cmd, loop, Loop } from 'redux-loop'

import { getRepoIssues, getUser, getUserRepos } from '../api/github'
import { getRepoIssueOrders, IssueOrder, updateRepoIssueOrders } from '../api/prioritizer'
import { Issue, OrderedIssue, Repo, Session } from '../types'
import { moveItemDown, moveItemUp, orderItems } from '../util/orderedItems'
import { getSession, persistSession, removeSession } from '../util/session'

import { Action, ActionType, AppState, State } from './types'

export const initialState: State = {
  appState: 'started',
  session: null,
  error: false,
  hasLoginError: false,
  repos: null,
  activeRepoId: null,
  issues: null,
}

/* Action Creators */

export function appStarted(): Action {
  return { type: ActionType.AppStarted }
}

function appInitialized(): Action {
  return { type: ActionType.AppInitialized }
}

function sessionFound(apiKey: string): Action {
  return { type: ActionType.SessionFound, apiKey }
}

function sessionStarted(session: Session): Action {
  return { type: ActionType.SessionStarted, session }
}

function sessionEnded(): Action {
  return { type: ActionType.SessionEnded }
}

export function errorOccurred(): Action {
  return { type: ActionType.ErrorOccurred }
}

export function errorCleared(): Action {
  return { type: ActionType.ErrorCleared }
}

export function loginSubmitted(apiKey: string): Action {
  return { type: ActionType.LoginSubmitted, apiKey }
}

function loginFailed(): Action {
  return { type: ActionType.LoginFailed }
}

export function loggedOut(): Action {
  return { type: ActionType.LoggedOut }
}

function reposLoaded(repos: Repo[]): Action {
  return { type: ActionType.ReposLoaded, repos }
}

export function repoSelected(repoId: number): Action {
  return { type: ActionType.RepoSelected, repoId }
}

export function repoDeselected(): Action {
  return { type: ActionType.RepoDeselected }
}

function issuesLoaded(issues: OrderedIssue[]): Action {
  return { type: ActionType.IssuesLoaded, issues }
}

export function issueShiftedUp(issueId: number): Action {
  return { type: ActionType.IssueShiftedUp, issueId }
}

export function issueShiftedDown(issueId: number): Action {
  return { type: ActionType.IssueShiftedDown, issueId }
}

/* Side Effects */
function wait(ms: number): Promise<void> {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

function checkSession(): Promise<string> {
  const apiKey = getSession()
  if (!apiKey) return Promise.reject()
  return Promise.resolve(apiKey)
}

function login(apiKey: string): Promise<Session> {
  return getUser(apiKey).then(user => ({ apiKey, user } as Session))
}

function loadIssues(repo: Repo, apiKey: string): Promise<OrderedIssue[]> {
  const issuesPromise = getRepoIssues(repo, apiKey)
  const issueOrdersPromise = getRepoIssueOrders(repo.id)

  return Promise.all([issuesPromise, issueOrdersPromise]).then(([issues, orders]) =>
    joinIssuesWithOrders(issues, orders),
  )
}

function joinIssuesWithOrders(issues: Issue[], issueOrders: IssueOrder[]): OrderedIssue[] {
  const orderByIssueId = issueOrders.reduce((acc, { issueId, order }) => {
    acc[issueId] = order
    return acc
  }, {} as { [key: number]: number })

  return orderItems(issues, issue => orderByIssueId[issue.id])
}

function updateIssueOrders(repoId: number, issues: OrderedIssue[]): Promise<void> {
  const issueOrders = issues.map(({ id, order }) => ({ issueId: id, order }))
  return updateRepoIssueOrders(repoId, issueOrders)
}

/* Reducer */

export function reducer(state: State = initialState, action: Action): State | Loop<State, Action> {
  switch (action.type) {
    case ActionType.AppStarted: {
      const cmd = Cmd.run(checkSession, {
        failActionCreator: appInitialized,
        successActionCreator: sessionFound,
      })

      return loop(state, cmd)
    }

    case ActionType.AppInitialized: {
      return { ...state, appState: 'initialized' }
    }

    case ActionType.SessionFound: {
      const updatedState = { ...state, appState: 'checking_session' as AppState }

      const cmd = Cmd.run(login, {
        args: [action.apiKey],
        successActionCreator: sessionStarted,
      })

      return loop(updatedState, cmd)
    }

    case ActionType.SessionStarted: {
      const updatedState = {
        ...state,
        appState: 'initialized' as AppState,
        session: action.session,
      }

      const cmd = Cmd.list([
        Cmd.run(persistSession, { args: [action.session.apiKey] }),
        Cmd.run(getUserRepos, {
          successActionCreator: reposLoaded,
          failActionCreator: errorOccurred,
          args: [action.session.apiKey],
        }),
      ])

      return loop(updatedState, cmd)
    }

    case ActionType.SessionEnded: {
      return { ...initialState, appState: 'initialized' }
    }

    case ActionType.ErrorOccurred: {
      const updatedState = { ...state, error: true }
      const cmd = Cmd.run(() => wait(3000), { successActionCreator: errorCleared })

      return loop(updatedState, cmd)
    }

    case ActionType.ErrorCleared: {
      return { ...state, error: false }
    }

    case ActionType.LoginSubmitted: {
      const updatedState = { ...state, hasLoginError: false }

      const cmd = Cmd.run(login, {
        args: [action.apiKey],
        failActionCreator: loginFailed,
        successActionCreator: sessionStarted,
      })

      return loop(updatedState, cmd)
    }

    case ActionType.LoginFailed: {
      return { ...state, hasLoginError: true }
    }

    case ActionType.LoggedOut: {
      const cmd = Cmd.run(removeSession, { successActionCreator: sessionEnded })

      return loop(state, cmd)
    }

    case ActionType.LoginErrorCleared: {
      return { ...state, hasLoginError: false }
    }

    case ActionType.ReposLoaded: {
      return { ...state, repos: action.repos }
    }

    case ActionType.RepoSelected: {
      const repo = state.repos && state.repos.find(({ id }) => id === action.repoId)

      if (!repo || !state.session) return state

      const updatedState = { ...state, activeRepoId: action.repoId, issues: null }

      const cmd = Cmd.run(loadIssues, {
        successActionCreator: issuesLoaded,
        failActionCreator: errorOccurred,
        args: [repo, state.session.apiKey],
      })

      return loop(updatedState, cmd)
    }

    case ActionType.RepoDeselected: {
      return { ...state, activeRepoId: null }
    }

    case ActionType.IssuesLoaded: {
      if (state.activeRepoId === null) return state

      const updatedState = { ...state, issues: action.issues }

      const cmd = Cmd.run(updateIssueOrders, { args: [state.activeRepoId, action.issues] })

      return loop(updatedState, cmd)
    }

    case ActionType.IssueShiftedUp: {
      const issue = state.issues && state.issues.find(({ id }) => id === action.issueId)
      if (!state.issues || !issue || state.activeRepoId === null) return state

      const issues = moveItemUp(state.issues, issue.order)

      const updatedState = { ...state, issues }

      const cmd = Cmd.run(updateIssueOrders, { args: [state.activeRepoId, issues] })

      return loop(updatedState, cmd)
    }

    case ActionType.IssueShiftedDown: {
      const issue = state.issues && state.issues.find(({ id }) => id === action.issueId)
      if (!state.issues || !issue || state.activeRepoId === null) return state

      const issues = moveItemDown(state.issues, issue.order)

      const updatedState = { ...state, issues }

      const cmd = Cmd.run(updateIssueOrders, { args: [state.activeRepoId, issues] })

      return loop(updatedState, cmd)
    }

    default: {
      return state
    }
  }
}
