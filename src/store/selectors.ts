import { Repo } from '../types'
import { State } from './types'

export function selectActiveRepo(state: State): Repo | null {
  return (
    (state.repos &&
      state.activeRepoId &&
      state.repos.find(repo => repo.id === state.activeRepoId)) ||
    null
  )
}

export function selectSortedIssues(state: State) {
  return state.issues && state.issues.slice().sort((a, b) => (a.order > b.order ? 1 : -1))
}
