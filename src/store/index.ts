import { compose, createStore } from 'redux'
import { install as installLoop, StoreCreator } from 'redux-loop'
import { initialState, reducer } from './reducer'

// Use Redux DevTools enhancer if not in production and extension found
// tslint:disable:no-any
const devToolsExtension =
  !process.env.PRODUCTION && (window as any).__REDUX_DEVTOOLS_EXTENSION__
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION__
    : null
// tslint:enable

const enhancers = devToolsExtension ? [installLoop(), devToolsExtension()] : [installLoop()]

export default (createStore as StoreCreator)(reducer, initialState, compose(...enhancers))
