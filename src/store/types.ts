import { OrderedIssue, Repo, Session, User } from '../types'

export type State = {
  appState: AppState
  session: Session | null
  error: boolean
  hasLoginError: boolean
  repos: Repo[] | null
  activeRepoId: number | null
  issues: OrderedIssue[] | null
}

export type AppState = 'started' | 'checking_session' | 'initialized'

export enum ActionType {
  AppStarted = 'AppStarted',
  AppInitialized = 'AppInitialized',
  SessionFound = 'SessionFound',
  SessionStarted = 'SessionStarted',
  SessionEnded = 'SessionEnded',
  ErrorOccurred = 'ErrorOccurred',
  ErrorCleared = 'ErrorCleared',
  LoginSubmitted = 'LoginSubmitted',
  LoggedIn = 'LoggedIn',
  LoginFailed = 'LoginFailed',
  LoggedOut = 'LoggedOut',
  LoginErrorCleared = 'LoginErrorCleared',
  ReposLoaded = 'ReposLoaded',
  RepoSelected = 'RepoSelected',
  RepoDeselected = 'RepoDeselected',
  IssuesLoaded = 'IssuesLoaded',
  IssueShiftedUp = 'IssueShiftedUp',
  IssueShiftedDown = 'IssueShiftedDown',
}

export type Action =
  | { type: typeof ActionType.AppStarted }
  | { type: typeof ActionType.AppInitialized }
  | { type: typeof ActionType.SessionFound; apiKey: string }
  | { type: typeof ActionType.SessionStarted; session: Session }
  | { type: typeof ActionType.SessionEnded }
  | { type: typeof ActionType.ErrorOccurred }
  | { type: typeof ActionType.ErrorCleared }
  | { type: typeof ActionType.LoginSubmitted; apiKey: string }
  | { type: typeof ActionType.LoggedIn; user: User }
  | { type: typeof ActionType.LoginFailed }
  | { type: typeof ActionType.LoggedOut }
  | { type: typeof ActionType.LoginErrorCleared }
  | { type: typeof ActionType.ReposLoaded; repos: Repo[] }
  | { type: typeof ActionType.RepoSelected; repoId: number }
  | { type: typeof ActionType.RepoDeselected }
  | { type: typeof ActionType.IssuesLoaded; issues: OrderedIssue[] }
  | { type: typeof ActionType.IssueShiftedUp; issueId: number }
  | { type: typeof ActionType.IssueShiftedDown; issueId: number }
