import * as React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import App from './view/App'
import GlobalStyles from './view/GlobalStyles'

const root = document.createElement('div')

document.body.appendChild(root)

render(
  <Provider store={store}>
    <GlobalStyles />
    <App />
  </Provider>,
  root,
)
