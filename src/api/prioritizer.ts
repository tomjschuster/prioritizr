export type IssueOrder = { issueId: number; order: number }

export function getRepoIssueOrders(repoId: number): Promise<IssueOrder[]> {
  return new Promise<IssueOrder[]>((resolve, reject) => {
    try {
      const issueOrdersJson = localStorage.getItem('issueOrder-' + repoId)

      if (!issueOrdersJson) {
        resolve([])
      } else {
        const issueOrders = JSON.parse(issueOrdersJson)
        resolve(issueOrders)
      }
    } catch (e) {
      reject(e)
    }
  })
}

export function updateRepoIssueOrders(repoId: number, issueOrders: IssueOrder[]): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    try {
      const issueOrdersJson = JSON.stringify(issueOrders)
      localStorage.setItem('issueOrder-' + repoId, issueOrdersJson)
      resolve()
    } catch (e) {
      reject(e)
    }
  })
}
