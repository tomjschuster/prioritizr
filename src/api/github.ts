import { Issue, Repo, User } from '../types'
import { http } from '../util/http'

const BASE_URL = 'https://api.github.com'

export function getUser(apiKey: string): Promise<User> {
  return get<User>('/user', apiKey)
}

export function getUserRepos(apiKey: string): Promise<Repo[]> {
  return get<Repo[]>('/user/repos?sort=pushed', apiKey)
}

export function getRepoIssues({ owner, name }: Repo, apiKey: string): Promise<Issue[]> {
  const url = `/repos/${owner.login}/${name}/issues`
  return get<Issue[]>(url, apiKey)
}

async function get<T>(path: string, apiKey: string): Promise<T> {
  const response = await http<T>(
    new Request(BASE_URL + path, { headers: { Authorization: 'token ' + apiKey } }),
  )

  return response.parsedBody !== undefined ? response.parsedBody : Promise.reject(response)
}
