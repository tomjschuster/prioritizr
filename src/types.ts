import { Orderable } from './util/orderedItems'

export type User = {
  login: string
  avatarUrl: string
  reposUrl: string
}

export type Repo = {
  id: number
  name: string
  htmlUrl: string
  openIssues: number
  owner: User
}

export type Issue = {
  id: number
  title: string
  htmlUrl: string
  assignee: User
  createdAt: string
  updatedAt: string
}

export type Session = {
  apiKey: string
  user: User
}

export type OrderedIssue = Orderable<Issue>
