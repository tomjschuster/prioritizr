export function formatDate(date: Date): string {
  const month = date.getMonth() + 1
  const day = date.getDate()
  const year = date.getFullYear()

  return `${padNum(month, 2)}/${padNum(day, 2)}/${year}`
}

function padNum(num: number, padCount: number) {
  return num.toString().padStart(padCount, '0')
}

const MS_IN_A_MINUTE = 1000 * 60
const MS_IN_AN_HOUR = MS_IN_A_MINUTE * 60
const MS_IN_A_DAY = MS_IN_AN_HOUR * 24
const MS_IN_A_YEAR = MS_IN_A_DAY * 365

export function formatTimeAgo(date: Date, from?: Date) {
  const msDiff = (from ? from.valueOf() : Date.now()) - date.valueOf()

  const years = Math.floor(msDiff / MS_IN_A_YEAR)
  if (years > 0) return formatTimeUnit(years, 'year')

  const days = Math.floor(msDiff / MS_IN_A_DAY)
  if (days > 0) return formatTimeUnit(days, 'day')

  const hours = Math.floor(msDiff / MS_IN_AN_HOUR)
  if (hours > 0) return formatTimeUnit(hours, 'hour')

  const minutes = Math.floor(msDiff / MS_IN_A_MINUTE)
  if (minutes > 0) return formatTimeUnit(minutes, 'minute')

  return 'just now'
}

function formatTimeUnit(count: number, unit: string): string {
  if (count === 1) return `${count} ${unit} ago`
  return `${count} ${unit}s ago`
}
