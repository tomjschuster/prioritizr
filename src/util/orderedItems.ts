export type Orderable<T> = { order: number } & T

export function moveItemUp<T>(items: Array<Orderable<T>>, order: number) {
  return swapItems(items, order, order - 1)
}

export function moveItemDown<T>(items: Array<Orderable<T>>, order: number) {
  return swapItems(items, order, order + 1)
}

function swapItems<T>(items: Array<Orderable<T>>, orderA: number, orderB: number) {
  if (orderA < 1 || orderB < 1 || orderA > items.length || orderB > items.length) {
    return items
  }

  return items.map(item => {
    if (item.order === orderA) return { ...item, order: orderB }
    if (item.order === orderB) return { ...item, order: orderA }
    return item
  })
}

export function orderItems<T>(
  items: T[],
  toOrder: (item: T) => number | undefined,
): Array<Orderable<T>> {
  return items
    .slice()
    .sort((a, b) => {
      const aOrder = toOrder(a)
      const bOrder = toOrder(b)

      if (aOrder === undefined && bOrder === undefined) return 0
      if (bOrder === undefined) return -1
      if (aOrder === undefined) return 1
      return aOrder > bOrder ? 1 : -1
    })
    .map((item: T, index: number): Orderable<T> => ({ ...item, order: index + 1 }))
}
