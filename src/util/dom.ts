import * as React from 'react'

export function useOutsideClick({ current }: React.RefObject<HTMLElement>, onClick: () => void) {
  function handleClickOutside({ target }: MouseEvent) {
    if (current && target instanceof HTMLElement && !current.contains(target)) {
      onClick()
    }
  }

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  })
}
