const SESSION_KEY = 'PRIORITIZER_API_KEY'

export function persistSession(apiKey: string): void {
  localStorage.setItem(SESSION_KEY, apiKey)
}

export function getSession(): string | null {
  return localStorage.getItem(SESSION_KEY)
}

export function removeSession(): void {
  localStorage.removeItem(SESSION_KEY)
}
