import { camel } from 'case'

export type HttpResponse<T> = Response & { parsedBody?: T }

export function http<T>(request: RequestInfo): Promise<HttpResponse<T>> {
  return new Promise((resolve, reject) => {
    let response: HttpResponse<T>
    fetch(request)
      .then(res => {
        response = res
        return res.json()
      })
      .then(body => {
        if (response.ok) {
          response.parsedBody = camelizeKeys(body)
          resolve(response)
        } else {
          reject(response)
        }
      })
      .catch(err => {
        reject(err)
      })
  })
}

export async function get<T>(
  path: string,
  args: RequestInit = { method: 'get' },
): Promise<HttpResponse<T>> {
  return await http<T>(new Request(path, args))
}

// tslint:disable:no-any
function camelizeKeys(value: any): any {
  if (Array.isArray(value)) return value.map(camelizeKeys)
  if (value === null || typeof value !== 'object') return value

  return Object.entries(value).reduce((acc: any, [k, v]: any) => {
    acc[camel(k)] = camelizeKeys(v)
    return acc
  }, ({} as {}) as any)
}
// tslint:enable
