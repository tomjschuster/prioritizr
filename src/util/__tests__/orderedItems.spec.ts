import { moveItemDown, moveItemUp, orderItems } from '../orderedItems'

describe('moveItemUp', () => {
  test('moves an item forward in the list', () => {
    const items = [{ order: 1 }, { order: 2 }, { order: 3 }]
    const newItems = moveItemUp(items, 2)

    expect(newItems[0].order).toBe(2)
    expect(newItems[1].order).toBe(1)
    expect(newItems[2].order).toBe(3)
  })

  test('leaves a list unchanged when trying to move first item', () => {
    const items = [{ order: 1 }, { order: 2 }, { order: 3 }]
    const newItems = moveItemUp(items, 1)

    expect(newItems[0].order).toBe(1)
    expect(newItems[1].order).toBe(2)
    expect(newItems[2].order).toBe(3)
  })
})

describe('moveItemDown', () => {
  test('moves an item back in the list', () => {
    const items = [{ order: 1 }, { order: 2 }, { order: 3 }]
    const newItems = moveItemDown(items, 2)

    expect(newItems[0].order).toBe(1)
    expect(newItems[1].order).toBe(3)
    expect(newItems[2].order).toBe(2)
  })

  test('leaves a list unchanged when trying to move last item', () => {
    const items = [{ order: 1 }, { order: 2 }, { order: 3 }]
    const newItems = moveItemDown(items, 3)

    expect(newItems[0].order).toBe(1)
    expect(newItems[1].order).toBe(2)
    expect(newItems[2].order).toBe(3)
  })
})

describe('orderItems', () => {
  test('adds order and sorts a list with the given function', () => {
    const items = [
      { id: 2, value: 2 },
      { id: 3, value: 3 },
      { id: 1, value: 1 },
    ]
    const newItems = orderItems(items, item => item.value)

    expect(newItems[0].order).toBe(1)
    expect(newItems[0].id).toBe(1)
    expect(newItems[1].order).toBe(2)
    expect(newItems[1].id).toBe(2)
    expect(newItems[2].order).toBe(3)
    expect(newItems[2].id).toBe(3)
  })

  test('moves items without sort values to end', () => {
    const items = [{ id: 2 }, { id: 3, value: 3 }, { id: 1, value: 1 }]
    const newItems = orderItems(items, item => item.value)

    expect(newItems[0].order).toBe(1)
    expect(newItems[0].id).toBe(1)
    expect(newItems[1].order).toBe(2)
    expect(newItems[1].id).toBe(3)
    expect(newItems[2].order).toBe(3)
    expect(newItems[2].id).toBe(2)
  })
})
