import { formatDate, formatTimeAgo } from '../dateTime'

describe('formatDate', () => {
  test('returns a string in mm/dd/yyyy format', () => {
    const date = new Date(2019, 11, 21)
    expect(formatDate(date)).toBe('12/21/2019')
  })

  test('pads month and day with zero', () => {
    const date = new Date(2019, 0, 3)
    expect(formatDate(date)).toBe('01/03/2019')
  })
})

describe('formatTimeAgo', () => {
  test('shows years ago when >= 1 year ago', () => {
    const from = new Date(2019, 11, 31)
    const date1 = new Date(2018, 11, 31)
    const date2 = new Date(2017, 11, 31)

    expect(formatTimeAgo(date1, from)).toBe('1 year ago')
    expect(formatTimeAgo(date2, from)).toBe('2 years ago')
  })

  test('shows days ago when < 1 year ago and >= 1 day ago', () => {
    const from = new Date(2019, 11, 31)
    const date1 = new Date(2019, 11, 30)
    const date2 = new Date(2019, 10, 31)
    const date3 = new Date(2019, 0, 1)

    expect(formatTimeAgo(date1, from)).toBe('1 day ago')
    expect(formatTimeAgo(date2, from)).toBe('30 days ago')
    expect(formatTimeAgo(date3, from)).toBe('364 days ago')
  })

  test('shows hours ago when < 1 day ago and >= 1 hour ago', () => {
    const from = new Date(2019, 11, 31, 23)
    const date1 = new Date(2019, 11, 31, 22)
    const date2 = new Date(2019, 11, 31, 21)
    const date3 = new Date(2019, 11, 31, 0)

    expect(formatTimeAgo(date1, from)).toBe('1 hour ago')
    expect(formatTimeAgo(date2, from)).toBe('2 hours ago')
    expect(formatTimeAgo(date3, from)).toBe('23 hours ago')
  })

  test('shows minutes ago when < 1 day ago and >= 1 minute ago', () => {
    const from = new Date(2019, 11, 31, 23, 59)
    const date1 = new Date(2019, 11, 31, 23, 58)
    const date2 = new Date(2019, 11, 31, 23, 57)
    const date3 = new Date(2019, 11, 31, 23, 0)

    expect(formatTimeAgo(date1, from)).toBe('1 minute ago')
    expect(formatTimeAgo(date2, from)).toBe('2 minutes ago')
    expect(formatTimeAgo(date3, from)).toBe('59 minutes ago')
  })

  test('shows just now when < 1 minute ago', () => {
    const from = new Date(2019, 11, 31, 23, 59, 59)
    const date1 = new Date(2019, 11, 31, 23, 59, 58)
    const date2 = new Date(2019, 11, 31, 23, 59, 0)

    expect(formatTimeAgo(date1, from)).toBe('just now')
    expect(formatTimeAgo(date2, from)).toBe('just now')
  })
})
