import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {
  issueShiftedDown,
  issueShiftedUp,
  repoDeselected,
  repoSelected,
} from '../../../store/reducer'
import { selectActiveRepo, selectSortedIssues } from '../../../store/selectors'
import { State } from '../../../store/types'
import { Session } from '../../../types'

import Layout from '../../common/Layout'

import IssueList from './components/IssueList'
import RepoList from './components/RepoList'

import { Container, IssueListContainer, RepoListContainer } from './Repos.styles'

export default function Repos({ session }: { session: Session }) {
  const repos = useSelector((state: State) => state.repos)
  const activeRepo = useSelector(selectActiveRepo)
  const issues = useSelector(selectSortedIssues)

  const dispatch = useDispatch()
  const handleRepoClick = (repoId: number) => dispatch(repoSelected(repoId))
  const handleDeselectRepoClick = () => dispatch(repoDeselected())
  const handleClickUp = (issueId: number) => dispatch(issueShiftedUp(issueId))
  const handleClickDown = (issueId: number) => dispatch(issueShiftedDown(issueId))

  if (!repos) return null

  return (
    <Layout session={session}>
      <Container>
        <RepoListContainer>
          <RepoList
            repos={repos}
            activeRepoId={activeRepo && activeRepo.id}
            onClickRepo={handleRepoClick}
          />
        </RepoListContainer>
        {activeRepo && (
          <IssueListContainer>
            <IssueList
              repo={activeRepo}
              issues={issues}
              onClickDeselectRepo={handleDeselectRepoClick}
              onClickUp={handleClickUp}
              onClickDown={handleClickDown}
            />
          </IssueListContainer>
        )}
      </Container>
    </Layout>
  )
}
