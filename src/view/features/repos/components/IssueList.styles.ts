import styled from 'styled-components'

import spacing from '../../../ui/spacing'
import { fontSize } from './../../../ui/constants'

export const Container = styled.section`
  border-top: 1px solid lightgrey;
  max-width: 800px;
  margin: 0 auto;
`
export const Header = styled.div`
  align-items: center;
  display: flex;
  padding: ${spacing.majorScale(2)}px;
`

export const Title = styled.h3`
  font-size: ${fontSize.large}px;
  margin-left: ${spacing.majorScale(6)}px;
`

export const RoundButton = styled.button`
  background-color: white;
  border: 1px solid black;
  border-radius: 50%;
  display: inline-flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: ${fontSize.normal}px;
  margin-left: ${spacing.majorScale(1)}px;
  padding: 0;
  width: 30px;
  height: 30px;
  text-align: center;

  &:hover:not(:disabled) {
    cursor: pointer;
    background-color: whitesmoke;
  }
`

export const StyledList = styled.ol`
  border-top: 1px solid lightgrey;
  margin: 0;
  padding: 0;
`

export const StyledListItem = styled.li`
  display: flex;
  justify-content: space-between;
  padding: ${spacing.majorScale(1)}px;
  border-bottom: 1px solid lightgrey;
`

export const AvatarContainer = styled.div`
  position: absolute;
`

export const IssueContent = styled.div`
  flex: 1;
  display: flex;
  margin-left: ${spacing.majorScale(6)}px;
  flex-direction: column;
`

export const IssueTitle = styled.p`
  font-weight: bold;
  margin: 0 0 ${spacing.minorScale(1)}px;
`
export const ReorderButtonsContainer = styled.div`
  width: 100px;
`

export const StyledAnchor = styled.a`
  text-decoration: none;
  color: black;

  &:hover {
    color: mediumseagreen;
  }
`
