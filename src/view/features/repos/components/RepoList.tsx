import * as React from 'react'

import { Repo } from '../../../../types'

import Avatar from '../../../common/Avatar'

import { Container, StyledList, StyledListItem, Title, TitleText } from './RepoList.styles'

type Props = {
  repos: Repo[]
  activeRepoId: number | null
  onClickRepo: (repoId: number) => void
}

export default function RepoList({ repos, activeRepoId, onClickRepo }: Props) {
  return (
    <Container>
      <Title>
        <TitleText>My GitHub Repos</TitleText> ({repos.length})
      </Title>
      <StyledList>
        {repos &&
          repos.map((repo: Repo) => (
            <StyledListItem
              key={repo.id}
              selected={repo.id === activeRepoId}
              onClick={() => repo.id !== activeRepoId && onClickRepo(repo.id)}
            >
              <Avatar src={repo.owner.avatarUrl} />
              {repo.owner.login}/{repo.name} ({repo.openIssues})
            </StyledListItem>
          ))}
      </StyledList>
    </Container>
  )
}
