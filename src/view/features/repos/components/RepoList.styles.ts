import styled from 'styled-components'

import spacing from '../../../ui/spacing'

export const Container = styled.section`
  max-width: 800px;
  margin: 0 auto;
`

export const Title = styled.h3`
  padding: 0 ${spacing.majorScale(2)}px;
  text-align: center;
`

export const TitleText = styled.span`
  color: seagreen;
`

export const StyledList = styled.ul`
  border-top: 1px solid lightgrey;
  list-style: none;
  margin: 0;
  padding: 0;
`

export const StyledListItem = styled.li<{ selected: boolean }>`
  align-items: center;
  background-color: ${({ selected }) => (selected ? 'powderblue' : 'white')};
  border-bottom: 1px solid lightgrey;
  cursor: ${({ selected }) => (selected ? 'default' : 'pointer')};
  display: flex;
  padding: ${spacing.majorScale(3)}px ${spacing.majorScale(2)}px;
  font-weight: ${({ selected }) => (selected ? 'bold' : 'normal')};

  &:hover {
    background-color: ${({ selected }) => (selected ? 'powderblue' : 'whitesmoke')};
  }
`
