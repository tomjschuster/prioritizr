import * as React from 'react'

import { OrderedIssue, Repo } from '../../../../types'
import { formatDate, formatTimeAgo } from '../../../../util/dateTime'

import Avatar from '../../../common/Avatar'

import {
  AvatarContainer,
  Container,
  Header,
  IssueContent,
  IssueTitle,
  ReorderButtonsContainer,
  RoundButton,
  StyledAnchor,
  StyledList,
  StyledListItem,
  Title,
} from './IssueList.styles'

type Props = {
  repo: Repo
  issues: OrderedIssue[] | null
  onClickDeselectRepo: () => void
  onClickUp: (issueId: number) => void
  onClickDown: (issueId: number) => void
}
export default function Repos(props: Props) {
  const { repo, issues, onClickDeselectRepo, onClickUp, onClickDown } = props

  return (
    <Container>
      <Header>
        <Title>
          <StyledAnchor href={repo.htmlUrl} target="_blank">
            {repo.owner.login}/{repo.name}
          </StyledAnchor>
        </Title>
        <RoundButton onClick={onClickDeselectRepo}>✖</RoundButton>
      </Header>
      <StyledList>
        {issues &&
          issues.map((issue: OrderedIssue) => (
            <StyledListItem key={issue.id}>
              {issue.assignee && (
                <AvatarContainer>
                  <Avatar src={issue.assignee.avatarUrl} />
                </AvatarContainer>
              )}

              <IssueContent>
                <IssueTitle>
                  {issue.order}.&nbsp;
                  <StyledAnchor href={issue.htmlUrl} target="_blank">
                    {issue.title}
                  </StyledAnchor>
                </IssueTitle>
                <div>
                  Created on {formatDate(new Date(Date.parse(issue.createdAt)))}, Updated{' '}
                  {formatTimeAgo(new Date(Date.parse(issue.updatedAt)))}.
                </div>
              </IssueContent>
              <ReorderButtonsContainer>
                <RoundButton disabled={issue.order === 1} onClick={() => onClickUp(issue.id)}>
                  ▲
                </RoundButton>
                <RoundButton
                  disabled={issue.order === issues.length}
                  onClick={() => onClickDown(issue.id)}
                >
                  ▼
                </RoundButton>
              </ReorderButtonsContainer>
            </StyledListItem>
          ))}
      </StyledList>
    </Container>
  )
}
