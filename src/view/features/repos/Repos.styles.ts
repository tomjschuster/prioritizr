import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  height: 100%;

  @media (min-width: 900px) {
    flex-direction: row;
  }
`

export const RepoListContainer = styled.div`
  flex: 1;
  overflow: scroll;
`

export const IssueListContainer = styled.div`
  flex: 2;
  overflow: scroll;
`
