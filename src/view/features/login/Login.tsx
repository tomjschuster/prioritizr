import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { loginSubmitted } from '../../../store/reducer'
import { State } from '../../../store/types'

import {
  ApiKeyContainer,
  ApiKeyInput,
  ApiKeyLabel,
  Container,
  GithubAnchor,
  Header,
  LoginButton,
  LoginError,
  Tagline,
  Title,
} from './Login.styles'

export default function Login() {
  const hasLoginError = useSelector((state: State) => state.hasLoginError)
  const dispatch = useDispatch()
  const [apiKey, setApiKey] = React.useState<string>('')

  const handleApiKeyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setApiKey(event.target.value)
  }

  const handleLoginSubmit = () => {
    dispatch(loginSubmitted(apiKey))
  }

  return (
    <Container>
      <Header>
        <Title>Prioritizr</Title>
        <Tagline>
          Order your GitHub issues the way <em>you</em> want them ordered.
        </Tagline>
      </Header>
      <ApiKeyContainer>
        <ApiKeyLabel htmlFor="api-key-input">
          Login using a{' '}
          <GithubAnchor href="https://github.com/settings/tokens" target="_blank">
            GitHub API key
          </GithubAnchor>
        </ApiKeyLabel>
        <ApiKeyInput
          id="api-key-input"
          value={apiKey}
          placeholder="Paste your API key here"
          onChange={handleApiKeyChange}
        />
      </ApiKeyContainer>
      <LoginButton onClick={handleLoginSubmit}>Login</LoginButton>
      {hasLoginError && <LoginError>Invalid API key</LoginError>}
    </Container>
  )
}
