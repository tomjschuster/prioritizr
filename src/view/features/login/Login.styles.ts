import styled from 'styled-components'

import { fontSize } from '../../ui/constants'
import spacing from '../../ui/spacing'

export const Container = styled.main`
  align-items: center;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  padding: ${spacing.majorScale(2)}px;
`

export const GithubAnchor = styled.a`
  color: mediumseagreen;
  text-decoration: none;

  &:hover {
    color: seagreen;
  }
`

export const Header = styled.header`
  margin-bottom: ${spacing.majorScale(4)}px;
`

export const Title = styled.h1`
  color: cornflowerblue;
  font-size: 48px;
  font-weight: lighter;
  letter-spacing: 0.25ch;
  margin: ${spacing.majorScale(1)}px 0;
  text-align: center;
  text-transform: uppercase;
`

export const Tagline = styled.p`
  font-size: 24px;
  margin: 0;
  text-align: center;
`

export const ApiKeyLabel = styled.label`
  font-size: ${fontSize.normal}px;
  font-weight: bold;
`

export const ApiKeyInput = styled.input`
  font-size: ${fontSize.normal}px;
  border: 1px solid black;
  border-radius: 2px;
  padding: ${spacing.majorScale(1)}px;
  text-align: center;
  width: 100%;
`

export const ApiKeyContainer = styled.div`
  align-items: center;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  margin-bottom: ${spacing.majorScale(2)}px;
  width: 350px;
  max-width: 100%;

  & > :not(:last-child) {
    margin-bottom: ${spacing.majorScale(1)}px;
  }
`
export const LoginButton = styled.button`
  background-color: white;
  border: 1px solid black;
  border-radiux: 4px;
  font-size: ${fontSize.large}px;
  padding: ${spacing.minorScale(1)}px ${spacing.majorScale(1)}px;
  text-transform: uppercase;

  &:hover {
    cursor: pointer;
    color: white;
    background-color: cornflowerblue;
  }
`

export const LoginError = styled.p`
  color: crimson;
`
