import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { appStarted } from '../store/reducer'
import { State } from '../store/types'

import Login from './features/login/Login'
import Repos from './features/repos/Repos'

const App: React.FC = () => {
  const appState = useSelector((state: State) => state.appState)
  const session = useSelector((state: State) => state.session)
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch(appStarted())
  }, [])

  if (appState === 'started') return null
  if (appState === 'checking_session') return null
  if (!session) return <Login />
  return <Repos session={session} />
}

export default App
