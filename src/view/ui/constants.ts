export const fontSize = {
  small: 12,
  normal: 16,
  large: 24,
  xlarge: 32,
}
