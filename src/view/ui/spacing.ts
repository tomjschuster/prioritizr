export function majorScale(scale: number) {
  return scale * 8
}

export function minorScale(scale: number) {
  return scale * 4
}

export default { majorScale, minorScale }
