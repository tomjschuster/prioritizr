import * as React from 'react'
import styled from 'styled-components'

const StyledImg = styled.img`
  height: 100%;
  margin-right: 4px
  width: auto;
`

const Container = styled.div<{ small?: boolean }>`
  height: ${({ small }) => (small ? 25 : 40)}px;
`

export default function Avatar({ src, small }: { src: string; small?: boolean }) {
  return (
    <Container small={small}>
      <StyledImg src={src} />
    </Container>
  )
}
