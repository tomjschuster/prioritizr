import styled from 'styled-components'

import { fontSize } from '../../ui/constants'
import spacing from '../../ui/spacing'

export const Container = styled.main`
  display: flex;
  flex-direction: column;
  height: 100vh;
`

export const Header = styled.header`
  flex-grow: 0;
  min-height: 60px;
  padding: 0 ${spacing.majorScale(1)}px;
  border-bottom: 1px solid lightgrey;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
`

export const HeaderContent = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  margin: auto;
  justify-content: space-between;
  max-width: 1200px;
`

export const Title = styled.h1`
  color: cornflowerblue;
  font-size: 24px;
  font-weight: lighter;
  letter-spacing: 0.25ch;
  margin: 0;
  text-transform: uppercase;
`
export const Content = styled.div`
  flex-grow: 1;
  overflow: hidden;
`

export const ErrorContainer = styled.div`
  background-color: white;
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  padding: ${spacing.majorScale(1)}px;
`

export const ErrorMessage = styled.p`
  font-size: ${fontSize.large}px;
  font-weight: bold;
`
