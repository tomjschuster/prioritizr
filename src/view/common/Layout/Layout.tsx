import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { loggedOut } from '../../../store/reducer'
import { State } from '../../../store/types'
import { Session } from '../../../types'

import UserDropdown from './components/UserDropdown'

import {
  Container,
  Content,
  ErrorContainer,
  ErrorMessage,
  Header,
  HeaderContent,
  Title,
} from './Layout.styles'

type Props = { session: Session; children: React.ReactNode }

export default function Layout({ session, children }: Props) {
  const error = useSelector((state: State) => state.error)
  const dispatch = useDispatch()

  const handleLogOutClick = () => dispatch(loggedOut())

  return (
    <Container>
      {error && (
        <ErrorContainer>
          <ErrorMessage>Sorry, something went wrong.</ErrorMessage>
        </ErrorContainer>
      )}
      <Header>
        <HeaderContent>
          <Title>Prioritizr</Title>
          <UserDropdown session={session} onLogOutClick={handleLogOutClick} />
        </HeaderContent>
      </Header>
      <Content>{children}</Content>
    </Container>
  )
}
