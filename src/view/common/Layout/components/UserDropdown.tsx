import * as React from 'react'

import { Session } from '../../../../types'
import { useOutsideClick } from '../../../../util/dom'

import Avatar from '../../Avatar'

import {
  Container,
  DropdownButton,
  DropdownButtonText,
  DropdownItem,
  DropdownMenu,
} from './UserDropdown.styles'

type Props = { session: Session; onLogOutClick: () => void }

export default function UserDropdown({ session, onLogOutClick }: Props) {
  const [open, setOpen] = React.useState<boolean>(false)
  const ref = React.useRef(null)
  useOutsideClick(ref, () => setOpen(false))

  return (
    <Container ref={ref}>
      <DropdownButton onClick={() => setOpen(!open)}>
        <Avatar small src={session.user.avatarUrl} />
        <DropdownButtonText>{session.user.login}</DropdownButtonText>
      </DropdownButton>
      {open && (
        <DropdownMenu>
          <DropdownItem onClick={onLogOutClick}>Logout</DropdownItem>
        </DropdownMenu>
      )}
    </Container>
  )
}
