import styled from 'styled-components'

export const Container = styled.div`
  align-items: center;
  display: flex;
  max-width: 40%;
  position: relative;
`
export const DropdownButton = styled.button`
  align-items: center;
  background: none;
  border: none;
  display: flex;
  cursor: pointer;
  padding: 0;
  width: 100%;

  &::after {
    content: '▼';
  }
`

export const DropdownButtonText = styled.span`
  font-size: 16px;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const DropdownMenu = styled.ul`
  background-color: white;
  box-sizing: border-box;
  padding: 0;
  position: absolute;
  bottom: 0;
  list-style: none;
  margin: 0;
  transform: translateY(100%);
  width: 100%;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
`

export const DropdownItem = styled.li`
  cursor: pointer;
  font-weight: bold;
  padding: 8px;

  &:not(:first-child) {
    border-top: 1px solid lightgrey;
  }

  &:hover {
    background-color: whitesmoke;
  }
`
